import { mapGetters, mapActions } from 'vuex';

export const placesGetters = {
  ...mapGetters('places', [
    'getPlaces',
    'getPlaceById',
    'getCurrentCount',
    'getTotalCount',
    'getCategories',
    'hasNextPage',
  ]),
};

export const placesActions = {
  ...mapActions('places', [
    'uploadImage',
    'fetchNextPage',
    'fetchPlacesCategories',
    'fetchPlacesWithQueries',
  ]),
};

export const auth = {
  ...mapActions('auth', ['signin', 'logOut']),
};

export const authGetters = {
  ...mapGetters('auth', ['getLoginStatus']),
};

export const eventGetters = {
  ...mapGetters('events', [
    'getEventById',
    'getEventList',
    'getCategoriesList',
    'hasNextPage',
    'getCurrentCount',
  ]),
};

export const geoGetters = {
  ...mapGetters('geo', ['getCities', 'getCountries']),
};

export const geoActions = {
  ...mapActions('geo', ['fetchGeoCountries', 'fetchGeoCities']),
};
