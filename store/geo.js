import axios from '../utils/axios-config';

export const state = () => ({
  geoCities: [],
  geoCountry: {},
});

export const getters = {
  getCities(state) {
    return state.geoCities;
  },

  getCountries: state => {
    return state.geoCountry;
  },
};

export const mutations = {
  GEO_CITIES(state, payload) {
    state.geoCities = payload;
  },
};

export const actions = {
  async fetchGeoCountries({ commit }) {
    let url = `/geo/cities/`;
    let cities = await axios.get('/places/');
    commit('GEO_CITIES', payload);
  },

  async fetchGeoCities({ commit }) {
    let url = `/geo/cities/`;
    let places = await axios.get(url);
    commit('GEO_CITIES', places.data.results);
  },
};
