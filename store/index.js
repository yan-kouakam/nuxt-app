export const state = () => {};

export const mutations = {};

export const actions = {
  async nuxtServerInit({ dispatch }) {
    try {
      await dispatch('events/init');
    } catch (e) {
      console.log(e);
    }
  },
};

export const getters = {};
