export const getAccessToken = req => {
  let token = null;
  let expiryDate = null;

  const access = JSON.parse(localStorage.getItem('access-token'));

  if (access) {
    token = access.access_token;
    expiryDate = access.expiry_date;
  }
  // } else {
  //   if (req && req.headers.cookie) {
  //     const cookieToken = req.headers.cookie.split('; ').find(function(cookie) {
  //       return cookie.trim().startsWith('access');
  //     });

  //     const expires = req.headers.cookie.split('; ').find(function(cookie) {
  //       return cookie.trim().startsWith('expiry');
  //     });

  //     if (cookieToken) {
  //       token = cookieToken.split('=')[1];
  //     }
  //     if (expires) {
  //       expiryDate = expires.split('=')[1];
  //     }
  //   }
  // }
  return { token, expiryDate };
};
