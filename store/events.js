import { Event } from '@/models/event';
import axios from '../utils/axios-config';

export const state = () => ({
  events: {
    results: [],
  },
  categories: {
    results: [],
  },
  selectedEvent: {},
});

export const mutations = {
  INIT_EVENT(state, events) {
    const results = events.results.map(evt => mapToEventModel(evt));
    state.events = { ...events, results };
  },

  CATEGORIES(state, categories) {
    state.categories = categories;
  },

  SELECTED_EVENT(state, event) {
    state.selectedEvent = event;
  },
};

export const actions = {
  async init({ commit }) {
    let events = {
      results: [],
      next: null,
      previous: null,
    };
    let response;
    try {
      const res = await axios.get('/events/');
      response = res.data;
    } catch (e) {
      console.log(e);
    }

    if (response && response.count > 0) {
      events = response;
    }
    commit('INIT_EVENT', events);
  },

  async fetchCategories({ commit }) {
    let categories;
    try {
      const response = await this.$axios.$get('/social/categories/');
      if (response && response.results.length > 0) {
        categories = response;
      }
    } catch (e) {
      console.log(e);
      categories = { results: ['None'] };
    }
    commit('CATEGORIES', categories);
  },

  async fetchSelectedEvent({ commit }, id) {
    try {
      const event = await this.$axios.$get(`/events/${id}`);
      commit('SELECTED_EVENT', event);
    } catch {
      console.log(e);
    }
  },
};

export const getters = {
  getSelectedEvent(state) {
    return state.selectedEvent;
  },
  getEventById: state => id => {
    const list = state.events.results.slice();
    return list.find(evt => evt.id === id);
  },

  getEventList(state) {
    return state.events.results.slice();
  },

  getCategoriesList(state) {
    return state.categories.results.slice();
  },

  getCurrentCount(state) {
    return state.events.results ? state.events.results.length : 0;
  },

  hasNextPage(state) {
    return state.events.next !== null;
  },
};

const mapToEventModel = e => {
  let {
    id,
    name,
    description,
    scope,
    thumbnail,
    time,
    categories,
    membership,
    location,
    slug,
    isFree,
  } = e;

  return new Event(
    id,
    name,
    description,
    scope,
    thumbnail,
    categories,
    membership,
    location,
    slug,
    isFree,
    time,
  );
};
