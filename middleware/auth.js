import { getAccessToken } from '@/utils/accessToken';

export default ({ req, app }) => {
  const { token, expiryDate } = getAccessToken(req);
  let isExpire = new Date(expiryDate) < Date.now();
  if (!token || isExpire) {
    app.router.push('/login');
  }
};
