import Vue from 'vue';
import Vuelidate from 'vuelidate';

const month = [
  'Jan',
  'Feb',
  'March',
  'April',
  'May',
  'Jun',
  'July',
  'Aug',
  'Sept',
  'Oct',
  'Nov',
  'Dec',
];

const dayOfWeek = ['Mon', 'Tu', 'Wed', 'Thur', 'Fri', 'Sat', 'Sun'];

const dateFilter = datetime => {
  const date = new Date(datetime);
  const year = date.getFullYear();
  const m = date.getMonth();
  const day = date.getUTCDate();
  const dayWeek = dayOfWeek[date.getUTCDay()];

  return `${dayWeek} ${day} ${month[m]} ${year}`;
};

const timeFilter = time => {
  let date = new Date(time);
  let hour = date.getUTCHours();
  let minutes = date.getUTCMinutes();

  if (minutes < 10) {
    minutes = 0 + '' + minutes;
  }
  if (hour <= 12) {
    hour = hour + ':' + minutes + ' AM';
  } else {
    hour = (hour % 12) + ':' + minutes + ' PM';
  }
  return hour;
};
Vue.config.productionTip = false;
Vue.use(Vuelidate);

Vue.filter('date', dateFilter);
Vue.filter('time', timeFilter);
