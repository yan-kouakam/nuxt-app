import { shallowMount } from '@vue/test-utils';
import PlaceSearchItem from '../search/PlaceSearchItem.vue';

describe('PlaceSearchItem.vue', () => {
  test('should be render the name in a h3 header', function() {
    const place = {
      name: 'Le cocotier',
    };
    const wrapper = shallowMount(PlaceSearchItem, {
      propsData: { place },
    });
    expect(wrapper.find('h3').text()).toBe(place.name);
  });

  test('should contains a city a street and a country in the li element', function() {
    const place = {
      city: 'Douala',
      street: 'rue de start',
      country: 'Douala',
    };
    const length = Object.keys(place).length;
    const wrapper = shallowMount(PlaceSearchItem, {
      propsData: { place },
    });

    expect(wrapper.findAll('li')).toHaveLength(length);
    expect(wrapper.props().place).toEqual(place);
  });
});
