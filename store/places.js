import { Place } from '@/models/place';
import axios from '../utils/axios-config';

export const state = () => ({
  places: {
    results: [],
  },
  selectedPlace: {},
  categories: { results: [] },
});

export const mutations = {
  LOAD_PLACES(state, rawPlaces) {
    const results = rawPlaces.results.map(place => mapToPlace(place));
    state.places = { ...rawPlaces, results };
  },

  NEXT_CHUNK(state, chuck) {
    state.places.next = chuck.next;
    state.places.previous = chuck.previous;
    chuck.results.forEach(element => {
      state.places.results.push(element);
    });
  },

  SELECTED_PLACE(state, selectedPlace) {
    state.selectedPlace = selectedPlace;
  },
  CATEGORIES(state, categories) {
    state.categories = categories;
  },
};

export const actions = {
  async fetchPlaces({ commit }) {
    try {
      let response = await axios.get('/places?radius=2000');

      commit('LOAD_PLACES', response.data);
    } catch (err) {
      console.log(`Error ${err}`);
    }
  },

  async fetchplaceById({ commit }, id) {
    try {
      const url = `/places/${id}`;
      const place = await axios.get(url);
      commit('SELECTED_PLACE', place.data);
    } catch (error) {
      console.log(error);
    }
  },

  async fetchNextPage({ commit, state }) {
    try {
      let next = state.places.next;
      let result;
      if (next) {
        next = next.replace(process.env.BASE_URL, '/');
        result = await axios.get(next);
        commit('NEXT_CHUNK', result.data);
      }
    } catch (err) {
      console.log(err);
    }
  },

  async fetchPlacesWithQueries({ commit }, queryParam) {
    try {
      const places = await axios.get('/places/?' + queryParam);
      commit('LOAD_PLACES', places.data);
    } catch (e) {
      console.log(e);
    }
  },

  async fetchPlacesCategories({ commit }) {
    try {
      const categories = await axios.get('/social/categories/');
      commit('CATEGORIES', categories.data);
    } catch (e) {
      console.log(e);
    }
  },

  uploadImage() {
    return 'not implemented';
  },
};

export const getters = {
  getPlaces: state => page => {
    let begin = 0;
    if (page !== 1) {
      begin = page * 10;
    }
    const end = begin + 10;
    return state.places.results.slice(begin, end);
  },

  getSelectedPlace(state) {
    return state.selectedPlace;
  },
  getPlaceById: state => id => {
    const places = state.places.results.slice();
    return places.find(place => place.id === id);
  },

  getCurrentCount(state) {
    return state.places.results ? state.places.results.length : 0;
  },

  getTotalCount(state) {
    return state.places.count ? state.places.count : 0;
  },

  getCategories(state) {
    return state.categories.results.slice();
  },

  hasNextPage(state) {
    return state.places.next !== null;
  },
};

const mapToPlace = rawPlace => {
  let {
    id,
    name,
    description,
    location,
    type,
    thumbnail,
    categories,
    slug,
  } = rawPlace;

  return new Place(
    id,
    name,
    description,
    location,
    type,
    thumbnail,
    categories,
    slug,
  );
};
