import _ from 'lodash';

export const state = () => ({
  me: {},
  events: {},
});

export const mutations = {
  INIT_USER(state, user) {
    state.me = _.mapKeys(user, (v, key) => _.camelCase(key));
  },

  EVENTS(state, events) {
    state.events = events;
  },
};

export const actions = {
  async fetchMe({ commit }) {
    try {
      const user = await this.$axios.$get('/accounts/users/me');
      commit('INIT_USER', user);
    } catch (e) {
      console.log(`could not fetch user me ${e.message}`);
    }
  },

  async fetchCreatedEvent({ commit, state }) {
    try {
      const events = await this.$axios.$get(
        `/events?creator=${state.me.id}&organizer=${state.me.id}`,
      );
      if (events) {
        commit('EVENTS', events);
      }
    } catch (e) {}
  },
};

export const getters = {
  getMyEvents(state) {
    if (!state.me.bookmarks) {
      return [];
    }
    return state.me.bookmarks.events.slice();
  },

  getMyPlaces(state) {
    if (!state.me.bookmarks) {
      return [];
    }
    return state.me.bookmarks.places.slice();
  },

  getUserId(state) {
    return state.me.id;
  },

  getCreatedEvents(state) {
    if (!state.events.results) {
      return [];
    }
    return state.events.results.slice();
  },

  getUser(state) {
    return state.me;
  },

  getAvatar(state) {
    return state.me.avatar;
  },
};
