import axios from '@/utils/axios-config';

export const uploadToAWS = async (params, file) => {
  const { host, ...p } = params;
  try {
    const response = await axios.put(params.url, file, {
      headers: {
        ...p,
        Host: host,
        'Content-type': p['content-type'],
      },
    });
    if (response.status === 200) {
      return response.config.url;
    }
  } catch (err) {
    return null;
  }
};
