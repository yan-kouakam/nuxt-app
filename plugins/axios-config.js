import { getAccessToken } from '@/utils/accessToken';

export default function({ $axios, req, env }) {
  $axios.setHeader('X-CLIENT-ID', env.CLIENT_ID);
  $axios.setHeader('X-CLIENT-SECRET', env.CLIENT_SECRET);
  let { token, expiryDate } = getAccessToken(req);

  if (token) {
    $axios.setHeader('Authorization', token);
  }
}
