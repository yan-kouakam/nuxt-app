import { shallowMount } from '@vue/test-utils';
import PlaceSearch from '../search/PlaceSearch.vue';
import PlaceSearchItemList from '../search/PlaceSearchItemList';
import { get } from '../../../__mocks__/axios';

jest.mock('axios');

describe('PlaceSearch.vue', () => {
  test('Render the input field', () => {
    const wrapper = shallowMount(PlaceSearch);
    expect(wrapper.contains('input')).toBe(true);
  });

  test('the list of place is initially hidden', () => {
    const wrapper = shallowMount(PlaceSearch);
    const container = wrapper.find('div.places').classes();

    expect(container).toContain('hidden');
    expect(wrapper.contains(PlaceSearchItemList)).toBe(true);
  });

  test('should fetch matching places async', async () => {});

  test('should update the list when the input changes', () => {
    const places = [
      {
        place: {
          name: 'Place one',
          city: 'Moscow',
          street: 'bolchaya',
          country: 'Russia',
        },
      },
      {
        place: {
          name: 'Place one',
          city: 'Moscow',
          street: 'bolchaya',
          country: 'Russia',
        },
      },
      {
        place: {
          name: 'Place one',
          city: 'Moscow',
          street: 'bolchaya',
          country: 'Russia',
        },
      },
      {
        place: {
          name: 'Place one',
          city: 'Moscow',
          street: 'bolchaya',
          country: 'Russia',
        },
      },
    ];
    const wrapper = shallowMount(PlaceSearch);
    const input = wrapper.find('input');
    input.element.value = 'Pla';
    input.trigger('input');
  });
});
