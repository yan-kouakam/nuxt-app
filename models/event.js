export class Event {
  id;
  name;
  description;
  scope;
  thumbnail;
  time;
  categories;
  membership;
  location;
  slug;
  isFree;

  constructor(
    id,
    name,
    description,
    scope,
    thumbnail,
    categories,
    membership,
    location,
    slug,
    isFree,
    time,
  ) {
    this.id = id;
    this.name = name;
    this.description = description;
    this.location = location;
    this.categories = categories;
    this.slug = slug;
    this.thumbnail = thumbnail;
    this.scope = scope;
    this.membership = membership;
    this.isFree = isFree;
    this.time = time;
  }
}

export class EventDetail extends Event {
  images;
  creator;
  place;
  organizer;
  chat;

  constructor(
    id,
    name,
    description,
    scope,
    thumbnail,
    categories,
    membership,
    location,
    slug,
    isFree,
    time,
    images,
    creator,
    place,
    organizer,
    chat,
  ) {
    super(
      id,
      name,
      description,
      scope,
      thumbnail,
      categories,
      membership,
      location,
      slug,
      isFree,
      time,
    );
    this.images = images;
    this.creator = creator;
    this.place = place;
    this.organizer = organizer;
    this.chat = chat;
  }
}
