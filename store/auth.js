/* eslint-disable no-undef */
import axios from '../utils/axios-config';
import { getAccessToken } from '@/utils/accessToken';

export const state = () => ({
  token: null,
});

export const mutations = {
  LOGIN(state, userToken) {
    state.token = userToken;
  },

  LOGOUT(state) {
    state.token = null;
  },
};

/*
 * Actions
 * in this part we can cause side effect since actions run asynchronously
 */
export const actions = {
  /**
   * make an API call to log the user in with the data provided
   * @param commit the callback (state mutation) to commit upon login successful
   * @param authData the email and password of the user
   */
  async signin({ commit }, authData) {
    const endpoint = 'accounts/obtain_token';
    try {
      const response = await axios.post(endpoint, authData);
      const data = response.data;
      data.socialUser = false;
      persistUserToken(data);
      commit('LOGIN', data);
      return true;
    } catch (er) {
      console.log(err);
      return false;
    }
  },

  register({ commit }, data) {
    return this.$axios.$post('/accounts/register', data).then(response => {
      let registrationToken = response.data.access_token;
      let expiryDate = response.data.expiry_date;
      localStorage.setItem(
        'registration-token',
        JSON.stringify({
          registration_token: registrationToken,
          expiry_date: expiryDate,
        }),
      );
      return true;
    });
  },
  /**
   *
   * @param commit
   * @param req
   */
  async initToken({ commit }) {
    let access = JSON.parse(localStorage.getItem('access-token'));
    let token = access.access_token;
    let expiryDate = access.expiry_date;

    if (token) {
      const date = new Date(expiryDate);
      if (date < new Date()) {
        // request a refresh token
        const data = {
          old_token: token,
        };
        const endpoint = process.env.BASE_URL + '/accounts/refresh_token';
        try {
          const res = await axios.post(endpoint, data, {
            headers: {
              'X-CLIENT-ID': process.env.CLIENT_ID,
              'X-CLIENT-SECRET': process.env.CLIENT_SECRET,
            },
          });
          token = res.data;
          persistUserToken(token);
          commit('LOGIN', token);
        } catch {
          console.log(err.message);
        }
      }
    }
    return token;
  },

  facebookLogin({ commit }) {
    return new Promise((resolve, reject) => {
      window.fbAsyncInit = function() {
        FB.init({
          appId: process.env.VUE_APP_FACEBOOK_ID,
          cookie: true, // enable cookies to allow the server to access
          // the session
          xfbml: true, // parse social plugins on this page
          version: 'v2.9', // use graph api version 2.8
        });
        FB.getLoginStatus(response => {
          console.log('login status', response);
          if (response.status === 'connected') {
            const data = {
              access_token: response.authResponse.accessToken,
              provider: 'facebook',
            };
            socialAuth(data, commit, resolve, reject);
          } else {
            FB.login(
              response => {
                if (response.status === 'connected') {
                  const data = {
                    access_token: response.authResponse.accessToken,
                    provider: 'facebook',
                  };
                  socialAuth(data, commit, resolve, reject);
                }
              },
              { scope: 'public_profile,email' },
            );
          }
        });
      };

      // Load the SDK asynchronously
      (function(d, s, id) {
        let js,
          fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s);
        js.id = id;
        js.src = 'https://connect.facebook.net/en_US/sdk.js';
        fjs.parentNode.insertBefore(js, fjs);
      })(document, 'script', 'facebook-jssdk');
    });
  },

  async googleSignIn({ commit }) {
    try {
      gapi.load('auth2', async () => {
        await gapi.auth2.init({
          client_id: process.env.VUE_APP_GOOGLE_API_ID,
          scope: 'profile email',
        });

        const GoogleAuth = gapi.auth2.getAuthInstance();
        let googleUser;

        if (GoogleAuth.isSignedIn.get()) {
          googleUser = await GoogleAuth.currentUser.get();
        } else {
          console.log('Google login failed');
        }
        googleUser = await GoogleAuth.signIn({
          scope: 'profile email',
        });
        const authData = await googleUser.getAuthResponse();

        const data = {
          access_token: authData.access_token,
          id_token: authData.id_token,
          provider: 'google-oauth2',
          state: 'parameter_pass_to_authorize_the_user',
        };

        const result = await axios.post('/accounts/social_auth', data);

        if (result) {
          result.socialUser = 'google';
          persistUserToken(result);
          commit('LOGIN', result);
          return Promise.resolve(true);
        }
      });
    } catch (err) {
      return Promise.reject(err);
    }
  },
  /**
   *
   * @param commit
   */
  logOut({ commit }) {
    const authData = localStorage.getItem('access-token');

    if (authData.socialUser === 'google') {
      gapi.load('auth2', async () => {
        await gapi.auth2.init({
          client_id: process.env.VUE_APP_GOOGLE_API_ID,
          scope: 'profile',
        });

        const GoogleAuth = gapi.auth2.getAuthInstance();
        await GoogleAuth.signOut();
      });
    }

    if (authData.socialUser === 'facebook') {
      window.fbAsyncInit = function() {
        FB.init({
          appId: process.env.VUE_APP_FACEBOOK_ID,
          cookie: true,
          xfbml: true,
          version: 'v2.9',
        });

        FB.logout();
      };
      (function(d, s, id) {
        let js,
          fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s);
        js.id = id;
        js.src = 'https://connect.facebook.net/en_US/sdk.js';
        fjs.parentNode.insertBefore(js, fjs);
      })(document, 'script', 'facebook-jssdk');
    }

    localStorage.removeItem('access-token');
    commit('LOGOUT');
    return true;
  },
};

export const getters = {
  getLoginStatus: state => {
    return state.token != null;
  },

  listRegisteredEvent: state => {
    return state.events;
  },
};

//
// ======= Helpers Function ======//
//

function persistUserToken(token) {
  localStorage.setItem('access-token', JSON.stringify(token));
}

function socialAuth(data, commit, resolve, reject) {
  axios
    .post('/accounts/social_auth', data)
    .then(response => {
      if (response) {
        response.data.socialUser = 'facebook';
        persistUserToken(response);
        commit('LOGIN', response);
        resolve(true);
      }
    })
    .catch(err => {
      reject(err);
    });
}
