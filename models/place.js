export class Place {
  id;
  name;
  description;
  location;
  type;
  thumbnail;
  categories;
  slug;

  constructor(
    id,
    name,
    description,
    location,
    type,
    thumbnail,
    categories,
    slug,
  ) {
    this.id = id;
    this.name = name;
    this.description = description;
    this.location = location;
    this.categories = categories;
    this.slug = slug;
    this.thumbnail = thumbnail;
    this.type = type;
  }
}

export class PlaceDetail extends Place {
  images;
  extraData;
  contact;

  constructor(
    id,
    name,
    description,
    location,
    type,
    thumbnail,
    categories,
    slug,
    images,
    extraData,
    contact,
  ) {
    super(
      id,
      name,
      description,
      location,
      type,
      thumbnail,
      categories,
      slug,
      images,
      extraData,
      contact,
    );
    this.images = images;
    this.contact = contact;
    this.extraData = extraData;
  }
}
