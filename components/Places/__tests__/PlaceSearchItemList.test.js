import { shallowMount } from '@vue/test-utils';
import PlaceSearchItemList from '../search/PlaceSearchItemList';
import PlaceSearchItem from '../search/PlaceSearchItem';

describe('PlaceSearchItemList', function() {
  test('should be initialize with and empty list', function() {
    const wrapper = shallowMount(PlaceSearchItemList, {});
    expect(wrapper.findAll(PlaceSearchItem)).toHaveLength(0);
  });

  test('should render the correct  places', () => {
    const places = [
      {
        place: {
          name: 'Place one',
          city: 'Moscow',
          street: 'bolchaya',
          country: 'Russia',
        },
      },
      {
        place: {
          name: 'Place one',
          city: 'Moscow',
          street: 'bolchaya',
          country: 'Russia',
        },
      },
      {
        place: {
          name: 'Place one',
          city: 'Moscow',
          street: 'bolchaya',
          country: 'Russia',
        },
      },
      {
        place: {
          name: 'Place one',
          city: 'Moscow',
          street: 'bolchaya',
          country: 'Russia',
        },
      },
    ];

    const wrapper = shallowMount(PlaceSearchItemList, {
      propsData: {
        places,
      },
    });
    const PlaceSearchItems = wrapper.findAll(PlaceSearchItem);

    expect(PlaceSearchItems).toHaveLength(places.length);

    PlaceSearchItems.wrappers.forEach((wrapper, i) => {
      expect(wrapper.props().place).toEqual(places[i]);
    });
  });
});
