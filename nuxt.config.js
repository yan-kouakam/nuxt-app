const pkg = require('./package');

module.exports = {
  mode: 'spa',

  /*
  ** Headers of the page
  */
  head: {
    title: pkg.name,
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: pkg.description },
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' },
      {
        rel: 'stylesheet',
        href:
          'https://fonts.googleapis.com/css?family=Lato:400,700|Open+Sans:400,700',
      },
    ],
  },

  /*
  ** Customize the progress-bar color
  */
  loading: '~/components/Spinner.vue',

  /*
  ** Global CSS
  */
  css: ['~assets/sass/main.scss'],

  /*
  ** Plugins to load before mounting the App
  */
  plugins: [
    '~plugins/_globals.js',
    '~plugins/main.js',
    '~plugins/axios-config.js',
  ],

  /*
  ** Nuxt.js modules
  */
  modules: [
    // Doc: https://github.com/nuxt-community/axios-module#usage
    '@nuxtjs/axios',
    '@nuxtjs/pwa',
    'bootstrap-vue/nuxt',
  ],
  /*
  ** Axios module configuration
  */
  axios: {
    // See https://github.com/nuxt-community/axios-module#options
    baseURL: 'https://weout-staging.herokuapp.com/api/v1',
  },

  /*
  ** Build configuration
  */
  build: {
    /*
    ** You can extend webpack config here
    */
    extend(config, ctx) {
      // Run ESLint on save
      if (ctx.isDev && ctx.isClient) {
        config.module.rules.push({
          enforce: 'pre',
          test: /\.(js|vue)$/,
          loader: 'eslint-loader',
          exclude: /(node_modules)/,
        });

        config.devtool = 'eval-source-map';
      }
    },
  },
  workbox: {
    dev: true,
  },
  manifest: {
    theme_color: '#009688',
    background_color: '#000',
  },
  env: {
    FACEBOOK_API_KEY: 'f6594646dd82826119767e0571c25137',

    FACEBOOK_ID: '1968301946529041',

    GOOGLE_API_KEY: 'MmZ58O149Qbx1kxtCaC2pd7k',

    GOOGLE_API_ID:
      '689889834726-hhvrj0pv8i3jl81te164717i1irsss1i.apps.googleusercontent.com',

    CLIENT_SECRET:
      'f599a6eddec5a27472982745218c6ca1d3cf97df2cdce6135f0ba1d8940066302295',

    CLIENT_ID: '2d2e2d5cbe68470992675279b004ff8f',

    BASE_URL: 'https://weout-staging.herokuapp.com/api/v1',
  },
};
