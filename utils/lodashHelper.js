import _ from 'lodash';

export const toCamelCase = obj => {
  return _.mapKeys(obj, (value, key) => _.camelCase(key));
};

export const toSnakeCase = obj => {
  return _.mapKeys(obj, (value, key) => _.snakeCase(key));
};
