import axios from 'axios';

const instance = axios.create({
  baseURL: process.env.BASE_URL,
  timeout: 5000,
  headers: {
    'X-CLIENT-ID': process.env.CLIENT_ID,
    'X-CLIENT-SECRET': process.env.CLIENT_SECRET,
  },
});

export default instance;
